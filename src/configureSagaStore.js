import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';

const configureSagaStore = (reducer, saga) => {
    const sagaMiddleware = createSagaMiddleware();
    let middleware = [sagaMiddleware];
    let composeEnhancers = compose;
    if (process.env.NODE_ENV !== 'production') {
        middleware.push(logger);
        composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    }
    const store = createStore(reducer, composeEnhancers(applyMiddleware(...middleware)));
    sagaMiddleware.run(saga);
    return store;
};

export default configureSagaStore;
