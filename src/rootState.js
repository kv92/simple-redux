//@flow
import type { InitState } from './init/init.type';
import type { LicenseState, AnalyticsState } from './types';
import type { TimeConfigState } from './timeConfig/timeConfig.type';
import type { AppConfigState } from './appConfig/appConfig.type';
import type { SettingsState } from './settings/settings.type';
import type { BoardState } from './board/board.type';
import type { BoardListState } from './boardList/boardList.type';
import type { SprintState } from './sprint/sprint.type';
import type { SprintReportState } from './sprintReport/sprintReport.type';

export type RootState = {
    init: InitState,
    license: LicenseState,
    analytics: AnalyticsState,
    timeConfig: TimeConfigState,
    appConfig: AppConfigState,
    settings: SettingsState,
    board: BoardState,
    boardList: BoardListState,
    sprint: SprintState,
    sprintReport: SprintReportState
};
