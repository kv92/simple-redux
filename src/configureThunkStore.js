import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

const configureStore = reducer => {
    let middleware = [thunk];
    let composeEnhancers = compose;
    if (process.env.NODE_ENV !== 'production') {
        middleware.push(logger);
        composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    }
    return createStore(reducer, composeEnhancers(applyMiddleware(...middleware)));
};

export default configureStore;
