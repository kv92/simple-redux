//@flow
import type { Saga } from '../types';
import type { License } from '../license/license.type';
import { call } from 'redux-saga/effects';
import { init } from './init/init.saga';
import { fetchLicense } from '../license/license.saga';

export default function* rootSaga(gadgetId: string): Saga {
    const license: License = yield call(fetchLicense);
    if (license.active) {
        yield call(init, gadgetId);
    }
}
